package com.example.labo2;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.ClipData;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Switch;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ListView lst;
    //private MenuItem mfruits;

    //public  final String entete_cat = getTitle().toString();
    private ArrayList<Produit> ProduitArrayList;


    private produitArrayAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //this.mfruits = (MenuItem) findViewById(R.id.idFruits);
        this.lst =(ListView) findViewById(R.id.lstProduit);
        this.ProduitArrayList = new ArrayList<Produit>();

        adapter = new produitArrayAdapter(this,R.layout.produit,ProduitArrayList);
        this.lst.setAdapter(adapter);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        MenuInflater minf = getMenuInflater();
        minf.inflate(R.menu.mon_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        super.onOptionsItemSelected(item);
        Intent i;
        //String textMenu = item.getTitle().toString();//recupere le titre de item selectionne  sur le menu ou champ text
        switch (item.getItemId())

        {
            case R.id.idFruits:
                i=new Intent(this,MainActivity2produit.class);
                //recupere le champ text du menu
               // String textMenu = item.getTitle().toString();
                i.putExtra("cat1",item.getTitle().toString());
                startActivity(i);
                return true;
            case R.id.idJus:
                i=new Intent(this,MainActivity2produit.class);
                //String textMenu1= item.getTitle().toString();
                //final String entete_cat = "fruits";
                i.putExtra("cat2",item.getTitle().toString());
                startActivity(i);
                return true;
            case R.id.idLegumes:
                i=new Intent(this,MainActivity2produit.class);
                //recupere le champ text du menu
                //textMenu = item.getTitle().toString();
                i.putExtra("cat3",item.getTitle().toString());
                startActivity(i);
                return true;
            case R.id.idlegumes:
                i=new Intent(this,MainActivity2produit.class);
                //textMenu = item.getTitle().toString();
                //final String entete_cat = "fruits";
                i.putExtra("cat4",item.getTitle().toString());
                startActivity(i);
                return true;
            case R.id.idPates:
                i=new Intent(this,MainActivity2produit.class);
               // textMenu = item.getTitle().toString();
                //final String entete_cat = "fruits";
                i.putExtra("cat5",item.getTitle().toString());
                startActivity(i);
                return true;
            case R.id.idViandes:
                i=new Intent(this,MainActivity2produit.class);
                //recupere le champ text du menu
                //textMenu = item.getTitle().toString();
                i.putExtra("cat6",item.getTitle().toString());
                startActivity(i);
                return true;


        }

        return super.onOptionsItemSelected(item);
    }
}