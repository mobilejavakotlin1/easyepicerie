package com.example.labo2;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import java.util.ArrayList;


public class produitTableDataGatWay extends SQLiteOpenHelper {

    private Context context;
    private SQLiteDatabase sqLiteDatabase;

    //Me

    public produitTableDataGatWay(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
        this.context=context;
    }



    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String query = "CREATE TABLE IF NOT EXISTS produits(id INTEGER PRIMARY KEY,img text, cat text,prix real,descy text);";
        sqLiteDatabase.execSQL(query);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        String query="DROP TABLE IF EXISTS produits;";
        sqLiteDatabase.execSQL(query);
        onCreate(sqLiteDatabase);
    }
    public void Open() { this.sqLiteDatabase = this.getWritableDatabase();}
    public  void Close() { this.sqLiteDatabase.close();}

    public void SeedFruitsDataBase() {
        //Fruits
        this.sqLiteDatabase.execSQL("INSERT INTO produits(img,cat, prix, descy) values ('700011','fruits',5,'raison');");
        this.sqLiteDatabase.execSQL("INSERT INTO produits(img,cat, prix, descy) values ('700010','fruits',3,'Banane');");
        this.sqLiteDatabase.execSQL("INSERT INTO produits(img,cat, prix, descy) values ('700009','fruits',4,'pomme');");
        this.sqLiteDatabase.execSQL("INSERT INTO produits(img,cat, prix, descy) values ('700008','fruits',9,'Mangue');");

        //Legume
        this.sqLiteDatabase.execSQL("INSERT INTO produits(img,cat, prix, descy) values ('700003','legumes',9,'Carotte');");
        this.sqLiteDatabase.execSQL("INSERT INTO produits(img,cat, prix, descy) values ('700005','legumes',4,'chou');");
        this.sqLiteDatabase.execSQL("INSERT INTO produits(img,cat, prix, descy) values ('700012','fruits',5,'Brocoli');");

    }



    //if()
    public ArrayList<Produit> SelectAllProduits()
    {
        //String cate=

       //String cat;
        ArrayList<Produit> listOfProduits = new ArrayList<Produit>();
        Cursor cursor = this.sqLiteDatabase.rawQuery("Select * from produits where cat ='fruits'",null);
        int idIndex = cursor.getColumnIndex("id");
        int imgIndex= cursor.getColumnIndex("img");
        int catIndex = cursor.getColumnIndex("cat");
        int prixIndex = cursor.getColumnIndex("prix");
        int descyIndex = cursor.getColumnIndex("descy");

        if((cursor!=null) && cursor.moveToFirst())
        {
            do{
                listOfProduits.add(new Produit(cursor.getInt(idIndex),cursor.getInt(imgIndex),cursor.getString(catIndex),cursor.getInt(prixIndex),cursor.getString(descyIndex)));
            }while (cursor.moveToNext());
        }

//        Produit pdt = new Produit(imgIndex,prixIndex,descIndex);
//        listOfProduits.add(pdt);
//        cursor.close();
        return listOfProduits;
    }


}
